$env:ASPNETCORE_ENVIRONMENT = "Development"

Start-Process "docker" -ArgumentList "run -it -p 27017:27017 mongo"

Start-Process "npm" -WorkingDirectory ".\ClientApp" -ArgumentList "start"

Start-Process "dotnet" -ArgumentList "watch run"