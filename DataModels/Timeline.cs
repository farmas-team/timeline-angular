using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace TimelineApp.DataModels
{
    public class Timeline
    {
        [BsonId]
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public long CreatedDate { get; set; }
        public List<TimelineGroup> Groups { get; set; }
        public List<string> Watchers { get; set; }
    }
}
