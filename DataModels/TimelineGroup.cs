using System;
using System.Collections.Generic;

namespace TimelineApp.DataModels
{
    public class TimelineGroup
    {
        public string Name { get; set; }

        public List<TimelineEvent> Events { get; set; } = new List<TimelineEvent>();
    }
}
