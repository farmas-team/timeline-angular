using System;

namespace TimelineApp.DataModels
{
    public class TimelineEvent
    {
        public string Text { get; set; }
        public string Caption { get; set; }
        public long Start { get; set; }
        public long End { get; set; }
    }
}
