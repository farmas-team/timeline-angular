using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimelineApp.DataModels;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Microsoft.Extensions.Configuration;
using System.Linq.Expressions;

namespace TimelineApp.Services
{
    public class MongoTimelineDataService : ITimelineDataService
    {
        private readonly MongoClient _client;
        private readonly IMongoDatabase _db;

        public MongoTimelineDataService(IConfiguration configuration)
        {
            var mongoUrl = configuration["database:mongodb:url"];
            var dbName = configuration["database:mongodb:dbname"] ?? "timeline";
            _client = new MongoClient(mongoUrl);
            _db = _client.GetDatabase(dbName);
        }

        public IMongoCollection<Timeline> Timelines
        {
            get
            {
                return _db.GetCollection<Timeline>("timelines");
            }
        }

        public async Task<IEnumerable<Timeline>> GetTimelinesAsync(Expression<Func<Timeline, bool>> predicate, bool expandGroups = false)
        {
            var find = Timelines.Find(predicate);

            if (!expandGroups)
            {
                find = find.Project<Timeline>(Builders<Timeline>.Projection.Exclude(t => t.Groups));
            }

            return await find.ToListAsync();
        }

        public async Task<Timeline> InsertTimelineAsync(Timeline timeline)
        {
            timeline.Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString();
            await Timelines.InsertOneAsync(timeline);
            return timeline;
        }

        public Task<Timeline> UpdateTimelineAsync(Timeline timeline)
        {
            var update = Builders<Timeline>.Update.Set(t => t.Title, timeline.Title);
            var options = new FindOneAndUpdateOptions<Timeline>
            {
                ReturnDocument = ReturnDocument.After
            };

            if (timeline.Groups != null)
            {
                update = update.Set(t => t.Groups, timeline.Groups);
            }

            if (timeline.Watchers != null)
            {
                update = update.Set(t => t.Watchers, timeline.Watchers);
            }

            return Timelines.FindOneAndUpdateAsync(
                Builders<Timeline>.Filter.Eq(t => t.Id, timeline.Id),
                update,
                options);
        }

        public Task DeleteTimelineAsync(string userId, string timelineId)
        {
            var builder = Builders<Timeline>.Filter;
            return Timelines.DeleteOneAsync(builder.Eq(t => t.Id, timelineId) & builder.Eq(t => t.UserId, userId));
        }
    }
}