using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TimelineApp.DataModels;

namespace TimelineApp.Services
{
    public class InMemoryTimelineDataService : ITimelineDataService
    {
        private static ConcurrentDictionary<string, Timeline> _timelines;

        static InMemoryTimelineDataService()
        {
            _timelines = new ConcurrentDictionary<string, Timeline>();
            _timelines.TryAdd("1", CreateTimeline("1", "My First Timeline (Server)"));
            _timelines.TryAdd("2", CreateTimeline("2", "My Second Timeline (Server)"));
        }

        public Task<IEnumerable<Timeline>> GetTimelinesAsync(Expression<Func<Timeline, bool>> predicate, bool expandGroups = false)
        {
            var timelines = _timelines.Values.Where(predicate.Compile());
            return Task.FromResult(timelines);
        }

        public Task<Timeline> InsertTimelineAsync(Timeline timeline)
        {
            timeline.Id = Guid.NewGuid().ToString();
            _timelines.TryAdd(timeline.Id, timeline);
            return Task.FromResult(timeline);
        }

        public Task DeleteTimelineAsync(string userId, string timelineId)
        {
            _timelines.Remove(timelineId, out var timeline);
            return Task.CompletedTask;
        }

        public Task<Timeline> UpdateTimelineAsync(Timeline timeline)
        {
            _timelines.AddOrUpdate(timeline.Id, timeline, (key, val) =>
            {
                val.Title = timeline.Title;
                val.Groups = timeline.Groups;
                return val;
            });
            return Task.FromResult(timeline);
        }

        private static Timeline CreateTimeline(string id, string title)
        {
            return new Timeline()
            {
                Id = id,
                Title = title,
                Groups = new List<TimelineGroup>()
                {
                    new TimelineGroup()
                    {
                        Name = "First Group",
                        Events = new List<TimelineEvent>()
                        {
                            new TimelineEvent()
                            {
                                Text = "Event 1",
                                Caption = "First Event",
                                Start = 1111,
                                End = 2222
                            },
                            new TimelineEvent()
                            {
                                Text = "Event 2",
                                Caption = "Second Event",
                                Start = 3333,
                                End = 4444
                            }
                        }
                    }
                }
            };
        }
    }
}