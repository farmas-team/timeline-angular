using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TimelineApp.DataModels;

namespace TimelineApp.Services
{
    public interface ITimelineDataService
    {
        Task<IEnumerable<Timeline>> GetTimelinesAsync(Expression<Func<Timeline, bool>> predicate, bool expandGroups = false);
        Task<Timeline> InsertTimelineAsync(Timeline timeline);
        Task DeleteTimelineAsync(string userId, string timelineId);
        Task<Timeline> UpdateTimelineAsync(Timeline timeline);
    }
}