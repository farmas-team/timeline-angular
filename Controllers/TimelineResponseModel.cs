using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TimelineApp.DataModels;

namespace TimelineApp.Controllers
{
    public class TimelineResponseModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public long CreatedDate { get; set; }
        public List<TimelineGroup> Groups { get; set; } = new List<TimelineGroup>();
        public List<string> Watchers { get; set; } = new List<string>();

        private TimelineResponseModel()
        {
        }

        public static TimelineResponseModel FromDataModel(Timeline timeline)
        {
            return new TimelineResponseModel()
            {
                Id = timeline.Id,
                Title = timeline.Title,
                UserId = timeline.UserId,
                Author = timeline.Author,
                CreatedDate = timeline.CreatedDate,
                Groups = timeline.Groups,
                Watchers = timeline.Watchers
            };
        }
    }
}