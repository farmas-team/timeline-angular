using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TimelineApp.DataModels;

namespace TimelineApp.Controllers
{
    public class TimelineInsertRequestModel
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public long CreatedDate { get; set; }
    }

    public class TimelineUpdateRequestModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public IEnumerable<TimelineGroupRequestModel> Groups { get; set; }

        public Timeline ToDataModel()
        {
            return new Timeline()
            {
                Id = this.Id,
                Title = this.Title,
                Groups = this.Groups.Select(g => g.ToDataModel()).ToList()
            };
        }
    }

    public class TimelineGroupRequestModel
    {
        public string Name { get; set; }

        [Required]
        public IEnumerable<TimelineEventRequestModel> Events { get; set; }

        public TimelineGroup ToDataModel()
        {
            return new TimelineGroup()
            {
                Name = this.Name,
                Events = this.Events.Select(e => e.ToDataModel()).ToList()
            };
        }
    }

    public class TimelineEventRequestModel
    {
        [Required]
        public string Text { get; set; }

        public string Caption { get; set; }

        [Required]
        public long Start { get; set; }

        [Required]
        public long End { get; set; }

        public TimelineEvent ToDataModel()
        {
            return new TimelineEvent()
            {
                Text = this.Text,
                Caption = this.Caption,
                Start = this.Start,
                End = this.End
            };
        }
    }
}