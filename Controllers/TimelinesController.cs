using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TimelineApp.DataModels;
using TimelineApp.Services;

namespace TimelineApp.Controllers
{
    [Authorize]
    [AutoValidateAntiforgeryToken]
    [Route("api/[controller]")]
    public class TimelinesController : Controller
    {
        private readonly ITimelineDataService _service;

        public TimelinesController(ITimelineDataService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<TimelineResponseModel>> Get()
        {
            var userId = GetUserId();
            var timelines = await _service.GetTimelinesAsync(timeline => timeline.UserId == userId);

            return timelines.Select(t => TimelineResponseModel.FromDataModel(t));
        }

        [HttpGet("tracked")]
        public async Task<IEnumerable<TimelineResponseModel>> Tracked()
        {
            var userId = GetUserId();
            var timelines = await _service.GetTimelinesAsync(timeline => timeline.Watchers != null && timeline.Watchers.Any(u => u == userId));

            return timelines.Select(t => TimelineResponseModel.FromDataModel(t));
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTimeline(string id)
        {
            var timelines = await _service.GetTimelinesAsync(t => t.Id == id, true);

            if (!timelines.Any())
            {
                return NotFound();
            }

            var timeline = TimelineResponseModel.FromDataModel(timelines.First());
            return Ok(timeline);
        }

        [HttpPost]
        public async Task<IActionResult> InsertTimeline([FromBody] TimelineInsertRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var timeline = new Timeline()
            {
                UserId = this.User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier)?.Value,
                Author = this.User.FindFirst(c => c.Type == ClaimTypes.Name)?.Value,
                Title = requestModel.Title,
                CreatedDate = requestModel.CreatedDate,
                Groups = new List<TimelineGroup>() { new TimelineGroup() },
                Watchers = new List<string>()
            };
            var response = await _service.InsertTimelineAsync(timeline);
            return Ok(response);
        }

        [HttpDelete("{timelineId}")]
        public async Task<IActionResult> DeleteTimeline(string timelineId)
        {
            var userId = GetUserId();
            await _service.DeleteTimelineAsync(userId, timelineId);
            return Ok();
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateTimeline([FromBody] TimelineUpdateRequestModel requestModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userId = GetUserId();
            var timeline = (await _service.GetTimelinesAsync(t => t.Id == requestModel.Id && t.UserId == userId)).FirstOrDefault();

            if (timeline == null)
            {
                return BadRequest();
            }

            var serverTimeline = await _service.UpdateTimelineAsync(requestModel.ToDataModel());
            return Ok(serverTimeline);
        }

        [HttpPost("{timelineId}/watchers")]
        public async Task<IActionResult> AddWatcher(string timelineId)
        {
            var timeline = (await _service.GetTimelinesAsync(t => t.Id == timelineId)).FirstOrDefault();
            var userId = GetUserId();

            if (timeline != null && !String.Equals(timeline.UserId, userId) && timeline.Watchers != null && !timeline.Watchers.Contains(userId))
            {
                timeline.Watchers.Add(userId);
                await _service.UpdateTimelineAsync(timeline);
            }

            return Ok();
        }

        [HttpDelete("{timelineId}/watchers")]
        public async Task<IActionResult> RemoveWatcher(string timelineId)
        {
            var timeline = (await _service.GetTimelinesAsync(t => t.Id == timelineId)).FirstOrDefault();
            var userId = GetUserId();

            if (timeline != null && timeline.Watchers != null)
            {
                timeline.Watchers.Remove(userId);
                await _service.UpdateTimelineAsync(timeline);
            }

            return Ok();
        }

        private string GetUserId()
        {
            return this.User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
        }
    }
}
