using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TimelineApp.DataModels;
using TimelineApp.Services;

namespace TimelineApp.Controllers
{
    public class AuthUser
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public bool IsAuthenticated { get; set; }
    }

    public class AuthController : Controller
    {
        private readonly IAntiforgery _xsrf;

        public AuthController(IAntiforgery xsrf)
        {
            this._xsrf = xsrf;
        }

        public Task<IActionResult> SignIn(string redirectUrl)
        {
            var properties = new AuthenticationProperties()
            {
                RedirectUri = Url.IsLocalUrl(redirectUrl) ? WebUtility.UrlDecode(redirectUrl) : "/"
            };
            return Task.FromResult<IActionResult>(Challenge(properties));
        }

        public async Task<IActionResult> SignOut()
        {
            await HttpContext.SignOutAsync();
            return Redirect("/");
        }

        public Task<AuthUser> GetCurrentUser()
        {
            var result = new AuthUser();

            if (this.User.Identity.IsAuthenticated)
            {
                result.Id = User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
                result.DisplayName = User.FindFirst(c => c.Type == ClaimTypes.Name)?.Value;
                result.IsAuthenticated = true;
            }

            var token = _xsrf.GetAndStoreTokens(this.HttpContext).RequestToken;
            this.HttpContext.Response.Cookies.Append("XSRF-TOKEN", token, new CookieOptions { HttpOnly = false });

            return Task.FromResult(result);
        }
    }
}
