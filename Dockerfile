# build image
FROM microsoft/dotnet:2.1-sdk as build
WORKDIR /app

# install node
RUN apt-get update && \
    apt-get install -y wget && \
    apt-get install -y gnupg2 && \
    wget -qO- https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y build-essential nodejs

COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o out

# runtime image
FROM microsoft/dotnet:2.1-aspnetcore-runtime

ENV PORT=5000

WORKDIR /app
COPY --from=build /app/out .
CMD ASPNETCORE_URLS=http://*:$PORT dotnet Timeline.Angular.dll