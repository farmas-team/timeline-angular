export interface Timeline {
  id: string;
  userId?: string;
  author?: string;
  createdDate?: number;
  title: string;
  groups: Group[];
  watchers?: string[];
}

export interface Group {
  name?: string;
  events: Event[];
}

export interface Event {
  text: string;
  caption?: string;
  start: number;
  end?: number;
}

export interface User {
  id: string;
  isAuthenticated: boolean;
  displayName: string;
}
