import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import * as toastr from 'toastr';

import { Timeline } from './app.typings';

const timelinesUrl = 'api/timelines';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TimelineHttpService {
  constructor(private http: HttpClient) { }

  addTimeline(title: string): Observable<Timeline> {
    var timeline: Timeline = {
      id: null,
      title: title,
      createdDate: new Date().getTime(),
      groups: null
    };

    return this.http.post<Timeline>(timelinesUrl, timeline, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        toastr.error(`Failed to insert timeline. Error: ${err.message}`);
        return of(null);
      })
    );
  }

  getTimelines(): Observable<Timeline[]> {
    return this.http.get<Timeline[]>(timelinesUrl).pipe(
      catchError((err: HttpErrorResponse) => {
        toastr.error(`Failed to retrieve timelines. Error: ${err.message}`);
        return of([]);
      })
    );
  }

  getTrackedTimelines(): Observable<Timeline[]> {
    const url = `${timelinesUrl}/tracked`;

    return this.http.get<Timeline[]>(url).pipe(
      catchError((err: HttpErrorResponse) => {
        toastr.error(`Failed to retrieve tracked timelines. Error: ${err.message}`);
        return of([]);
      })
    );
  }

  getTimeline(id: string): Observable<Timeline> {
    const url = `${timelinesUrl}/${id}`;

    return this.http.get<Timeline>(url).pipe(
      catchError((err: HttpErrorResponse) => {
        toastr.error(`Failed to retrieve timeline ${id}. Error: ${err.message}`);

        if (err.status === 404) {
          return of(null)
        } else {
          return Observable.throw(err);
        }
      })
    );
  }

  updateTimeline(timeline: Timeline): Observable<any> {
    const url = `${timelinesUrl}/${timeline.id}`;

    return this.http.post<Timeline>(url, timeline, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        toastr.error(`Failed to update timeline ${timeline.id}. Error: ${err.message}`);
        return Observable.throw(err);
      })
    );
  }

  beginWatching(timelineId: string): Observable<any> {
    const url = `${timelinesUrl}/${timelineId}/watchers`;

    return this.http.post(url, null, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        toastr.error(`Failed to track timeline ${timelineId}. Error: ${err.message}`);
        return Observable.throw(err);
      })
    );
  }

  stopWatching(timelineId: string): Observable<any> {
    const url = `${timelinesUrl}/${timelineId}/watchers`;

    return this.http.delete(url, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        toastr.error(`Failed to stop tracking timeline ${timelineId}. Error: ${err.message}`);
        return Observable.throw(err);
      })
    );
  }

  deleteTimeline(id: string): Observable<any> {
    const url = `${timelinesUrl}/${id}`;

    return this.http.delete(url, httpOptions).pipe(
      catchError((err: HttpErrorResponse) => {
        toastr.error(`Failed to delete timeline ${id}. Error: ${err.message}`);
        return of(null);
      })
    );
  }
}
