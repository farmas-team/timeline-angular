import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, AfterViewInit {
  @ViewChild('modalHeader') modalHeader: ElementRef;
  @ViewChild('modalBody') modalBody: ElementRef;

  constructor(private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.showHeader = this.modalHeader.nativeElement && this.modalHeader.nativeElement.children.length > 0;
    this.changeDetector.detectChanges();
  }

  public visible = false;
  public visibleAnimate = false;
  public showHeader = true;

  public show(): void {
    this.visible = true;

    setTimeout(() => {
      this.visibleAnimate = true;
      $(this.modalBody.nativeElement).find('.app-modal-focus').focus();
    }, 100);
  }

  public hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  public onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('modal')) {
      this.hide();
    }
  }
}
