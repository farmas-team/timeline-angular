import { Injectable } from '@angular/core';
import { DataSet, Timeline as visTimeline, TimelineOptions } from 'vis';
import * as moment from 'moment';
import * as $ from 'jquery';

import { Timeline, Group, Event, User } from './app.typings';

export interface TimelineUI {
  destroy(): void;
  onInitialDrawComplete(callback: () => void): void;
}

@Injectable({
  providedIn: 'root'
})
export class TimelineRenderService {

  constructor() { }

  renderTimeline(timeline: Timeline, container: any): TimelineUI {
    var groups: { id: string, content: string }[] = [];
    var items = new DataSet();
    var minDate: number = null;
    var maxDate: number = null;

    timeline.groups.forEach((group, index) => {
      let groupId = index.toString();
      groups.push({ id: groupId, content: group.name });
      group.events.forEach(e => {
        if (minDate === null || e.start < minDate) {
          minDate = e.start;
        }

        // todo: change this when using range dates.
        if (maxDate === null || e.start > maxDate) {
          maxDate = e.start;
        }

        items.add({
          content: e.text,
          title: e.caption,
          group: groupId,
          start: new Date(e.start),
          end: new Date(e.end)
        })
      });
    });

    // Configuration for the Timeline
    let timelineStart = moment(new Date(minDate)).subtract(1, 'years').toDate();
    let timelineEnd = moment(new Date(maxDate)).add(1, 'years').toDate();

    let callbacks = $.Callbacks('memory');
    let options: TimelineOptions = {
      start: timelineStart,
      horizontalScroll: true,
      zoomKey: 'ctrlKey',
      showMinorLabels: true,
      type: 'point'
    };

    (<any>options).onInitialDrawComplete = () => {
      callbacks.fire();
    }

    // Create a Timeline
    var timelineUI = new visTimeline(container, items, groups, options);

    return {
      destroy: () => {
        timelineUI.destroy();
      },
      onInitialDrawComplete: (callback: () => void) => {
        callbacks.add(callback);
      }
    }
  }
}
