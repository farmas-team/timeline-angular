import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { User } from '../app.typings';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public user: User;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.getUserInfo().subscribe(u => this.user = u);
  }
}
