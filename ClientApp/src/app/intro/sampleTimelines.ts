import { Timeline, Group, Event } from '../app.typings';
import * as moment from 'moment';

export const TechnologyGroup: Group = {
  name: 'Technology',
  events: [
    {
      text: 'Italian Alessandro Volta makes the first battery(known as a Voltaic pile)',
      start: new Date(1800, 1).getTime(),
      end: new Date(1800, 1).getTime()
    },
    {
      text: 'George Stephenson builds the first practical steam locomotive.',
      start: new Date(1814, 1).getTime(),
      end: new Date(1814, 1).getTime()
    },
    {
      text: 'Michael Faraday builds primitive electric generators and motors.',
      start: new Date(1820, 1).getTime(),
      end: new Date(1820, 1).getTime()
    },
    {
      text: 'William Sturgeon develops the first practical electric motor.',
      start: new Date(1830, 1).getTime(),
      end: new Date(1830, 1).getTime()
    },
    {
      text: 'Scotsman Alexander Bain invents a primitive fax machine based on chemical technology.',
      start: new Date(1840, 1).getTime(),
      end: new Date(1840, 1).getTime()
    },
    {
      text: 'Louis Pasteur develops pasteurization: a way of preserving food by heating it to kill off bacteria.',
      start: new Date(1850, 1).getTime(),
      end: new Date(1850, 1).getTime()
    },
    {
      text: 'Frenchman Étienne Lenoir and German Nikolaus Otto pioneer the internal combustion engine.',
      start: new Date(1860, 1).getTime(),
      end: new Date(1860, 1).getTime()
    },
    {
      text: 'Thomas Edison develops the phonograph, the first practical method of recording and playing back sound on metal foil.',
      start: new Date(1870, 1).getTime(),
      end: new Date(1870, 1).getTime()
    },
    {
      text: 'Thomas Edison patents the modern incandescent electric lamp.',
      start: new Date(1880, 1).getTime(),
      end: new Date(1880, 1).getTime()
    },
    {
      text: 'German engineer Rudolf Diesel develops his diesel engine—a more efficient internal combustion engine without a sparking plug.',
      start: new Date(1890, 1).getTime(),
      end: new Date(1890, 1).getTime()
    },
    /*
    {
      text: '',
      start: new Date(19, 1).getTime(),
      end: new Date(19, 1).getTime()
    },
    */
  ]
}
