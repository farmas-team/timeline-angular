import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { TimelineRenderService, TimelineUI } from '../timeline-render.service';
import * as $ from 'jquery';

import { TechnologyGroup } from './sampleTimelines';
import { Timeline } from '../app.typings';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("timelineDiv") timelineDiv: ElementRef;

  private _timelineUI: TimelineUI;

  constructor(private renderService: TimelineRenderService) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    var container = this.timelineDiv.nativeElement;

    var timeline: Timeline = {
      id: 'sample',
      title: 'Sample Timeline',
      groups: [TechnologyGroup]
    };

    this._timelineUI = this.renderService.renderTimeline(timeline, container);
    this._timelineUI.onInitialDrawComplete(() => {
      $('.timeline-loading').hide();
    });
  }

  ngOnDestroy() {
    this._timelineUI && this._timelineUI.destroy();
  }
}
