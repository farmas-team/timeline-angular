import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, NavigationEnd, Event } from '@angular/router';

import { User } from '../app.typings';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public user: User;
  public url: string = "/";

  constructor(private authService: AuthService, router: Router) {
    router.events.subscribe((evt: Event) => {
      if (evt instanceof NavigationEnd) {
        this.url = encodeURI(router.url);
      }
    });
  }

  ngOnInit() {
    this.authService.getUserInfo().subscribe(u => this.user = u);
  }
}
