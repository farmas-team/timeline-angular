import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { forkJoin } from 'rxjs';
import * as toastr from 'toastr';

import { AuthService } from '../auth.service';
import { Timeline, Group, Event, User } from '../app.typings';
import { TimelineHttpService } from '../timeline-http.service';
import { TimelineRenderService, TimelineUI } from '../timeline-render.service';
import { EditableGroup, EditableEvent } from './editableTimeline'

@Component({
  selector: 'app-timeline-detail',
  templateUrl: './timeline-detail.component.html',
  styleUrls: ['./timeline-detail.component.css']
})
export class TimelineDetailComponent implements OnInit, OnDestroy {
  @ViewChild('timelineDiv') timelineDiv: ElementRef;
  @ViewChild('eventForm') eventForm: FormGroup;

  private _timelineUI: TimelineUI;
  private _activeGroup: EditableGroup;
  private _id: string;
  private _user: User;

  public title: string;
  public groups: EditableGroup[] = [];
  public newEvent: EditableEvent;
  public isEnabled: boolean = false;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    public location: Location,
    private timelineHttpService: TimelineHttpService,
    private timelineRenderService: TimelineRenderService) {
    this.newEvent = new EditableEvent();
  }

  ngOnInit() {
    this._id = this.route.snapshot.paramMap.get('id');

    forkJoin(
      this.timelineHttpService.getTimeline(this._id),
      this.authService.getUserInfo()
    ).subscribe(res => {
      var timeline = res[0];
      if (!timeline) {
        this.router.navigateByUrl('404');
      } else {
        this.title = timeline.title;
        this.groups = timeline.groups.map<EditableGroup>(group => EditableGroup.fromDataModel(group));
      }

      this._user = res[1];
      this.isEnabled = timeline && this._user && this._user.id === timeline.userId;
      if (timeline && !this.isEnabled) {
        toastr.error('You are not authorized to edit this timeline.');
      }
    });
  }

  ngOnDestroy(): void {
    this._timelineUI && this._timelineUI.destroy();
  }

  addGroup(): void {
    this.groups.push(new EditableGroup());
  }

  removeGroup(group: EditableGroup): void {
    this.groups = this.groups.filter(g => g !== group);
  }

  activateGroup(group: EditableGroup): void {
    this._activeGroup = group;
    this.newEvent = new EditableEvent();
    this.eventForm.reset({ 'eventStart': this.newEvent.startDate, 'eventEnd': this.newEvent.endDate });
  }

  addEvent(): void {
    let theEvent = new EditableEvent(this.newEvent.text, this.newEvent.caption, this.newEvent.startDate, this.newEvent.endDate);
    this._activeGroup && this._activeGroup.events.push(theEvent);

    this.newEvent.text = '';
    this.newEvent.caption = '';
  }

  removeEvent(group: EditableGroup, event: EditableEvent): void {
    group.events = group.events.filter(e => e !== event);
  }

  saveAndRender(): void {
    this._timelineUI && this._timelineUI.destroy();

    let timeline = this._toDataModel();
    this._timelineUI = this.timelineRenderService.renderTimeline(timeline, this.timelineDiv.nativeElement);

    this.timelineHttpService.updateTimeline(timeline).subscribe(_ => {
      toastr.success('Timeline has been saved.');
    });
  }

  share() {
    var url = window.location.origin + '/timelines/' + this._id;
    toastr.info(`Link to your timeline: <b><a href="${url}">${url}</a></b>`);
  }

  private _toDataModel(): Timeline {
    return {
      id: this._id,
      title: this.title,
      groups: this.groups.map<Group>(g => g.toDataModel())
    };
  }
}
