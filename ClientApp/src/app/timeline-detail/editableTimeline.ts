import * as moment from 'moment';
import { Timeline, Group, Event } from '../app.typings';

const dateFormat = 'YYYY-MM-DD';
const today = moment().format(dateFormat);
const tomorrow = moment().add(1, 'd').format(dateFormat);

export class EditableGroup {
  constructor(public name: string = '', public events: EditableEvent[] = []) { }

  toDataModel(): Group {
    return {
      name: this.name || '',
      events: this.events.map(e => e.toDataModel())
    }
  }

  static fromDataModel(group: Group): EditableGroup {
    return new EditableGroup(
      group.name,
      group.events.map<EditableEvent>(evt => EditableEvent.fromDataModel(evt)));
  }
}

export class EditableEvent {
  constructor(public text: string = '', public caption: string = '', public startDate: string = today, public endDate: string = tomorrow) { }

  toDataModel(): Event {
    return {
      text: this.text,
      caption: this.caption,
      start: moment(this.startDate, dateFormat).toDate().getTime(),
      end: moment(this.endDate, dateFormat).toDate().getTime()
    }
  }

  static fromDataModel(evt: Event): EditableEvent {
    return new EditableEvent(
      evt.text,
      evt.caption,
      moment(new Date(evt.start)).format(dateFormat),
      moment(new Date(evt.end)).format(dateFormat));
  }
}
