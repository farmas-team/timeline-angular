import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import * as moment from 'moment';
import * as toastr from 'toastr';
import * as $ from 'jquery';

import { TimelineHttpService } from '../timeline-http.service';
import { TimelineRenderService, TimelineUI } from '../timeline-render.service';
import { AuthService } from '../auth.service';
import { Timeline, User } from '../app.typings';

@Component({
  selector: 'app-timeline-render',
  templateUrl: './timeline-render.component.html',
  styleUrls: ['./timeline-render.component.css']
})
export class TimelineRenderComponent implements OnInit, OnDestroy {
  @ViewChild('timelineDiv') timelineDiv: ElementRef;

  private _timelineUI: TimelineUI;

  public timeline: Timeline;
  public user: User;
  public isTracking: boolean;

  constructor(
    private timelineHttpService: TimelineHttpService,
    private timelineRenderService: TimelineRenderService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');

    forkJoin(
      this.timelineHttpService.getTimeline(id),
      this.authService.getUserInfo()
    ).subscribe(res => {
      this.timeline = res[0];
      this.user = res[1];

      if (!this.timeline) {
        this.router.navigateByUrl('404');
      } else {
        this.isTracking = !!this.timeline.watchers && this.timeline.watchers.some(w => w === this.user.id);
        this._timelineUI = this.timelineRenderService.renderTimeline(this.timeline, this.timelineDiv.nativeElement);
        this._timelineUI.onInitialDrawComplete(() => {
          $('.timeline-loading').hide();
        });
      }
    });
  }

  ngOnDestroy() {
    this._timelineUI && this._timelineUI.destroy();
  }

  beginTracking() {
    if (!!this.timeline) {
      this.timelineHttpService.beginWatching(this.timeline.id).subscribe(_ => {
        toastr.success('Timeline has been added to your list.');
        this.isTracking = true;
      });
    }
  }

  stopTracking() {
    if (!!this.timeline) {
      this.timelineHttpService.stopWatching(this.timeline.id).subscribe(_ => {
        toastr.success('Timeline has been removed from your list.');
        this.isTracking = false;
      });
    }
  }

  formatDate(date: number): string {
    if (!!date) {
      return moment(new Date(date)).fromNow();
    }
  }
}
