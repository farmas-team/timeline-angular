import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { IntroComponent } from './intro/intro.component';
import { TimelinesComponent } from './timelines/timelines.component';
import { TimelineDetailComponent } from './timeline-detail/timeline-detail.component';
import { ModalComponent } from './modal/modal.component';
import { TimelineHttpService } from './timeline-http.service';
import { AuthService } from './auth.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { TimelineRenderComponent } from './timeline-render/timeline-render.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    IntroComponent,
    TimelinesComponent,
    TimelineDetailComponent,
    ModalComponent,
    NotFoundComponent,
    HomeComponent,
    TimelineRenderComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'timelines', component: TimelinesComponent },
      { path: 'timelines/:id/edit', component: TimelineDetailComponent },
      { path: 'timelines/:id', component: TimelineRenderComponent },
      { path: 'about', component: AboutComponent },
      { path: '404', component: NotFoundComponent },
    ])
  ],
  providers: [TimelineHttpService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
