import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { User } from './app.typings';

let userObservable: Observable<User>;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  getUserInfo(): Observable<User> {
    if (!userObservable) {
      userObservable = this.http.get<User>('/auth/getcurrentuser', httpOptions).pipe(
        catchError(err => of({ id: '', isAuthenticated: false, displayName: '' }))
      );
    }

    return userObservable;
  }
}
