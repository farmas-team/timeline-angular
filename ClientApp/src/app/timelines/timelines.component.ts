import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import * as toastr from 'toastr';
import * as moment from 'moment';

import { TimelineHttpService } from '../timeline-http.service';
import { Timeline } from '../app.typings';

@Component({
  selector: 'app-timelines',
  templateUrl: './timelines.component.html',
  styleUrls: ['./timelines.component.css']
})
export class TimelinesComponent implements OnInit {

  public timelines: Timeline[];
  public trackedTimelines: Timeline[];
  public newTitle: string;

  constructor(private router: Router, private timelineService: TimelineHttpService) { }

  ngOnInit() {
    forkJoin(
      this.timelineService.getTimelines(),
      this.timelineService.getTrackedTimelines()
    ).subscribe(res => {
      this.timelines = res[0];
      this.trackedTimelines = res[1];
    });
  }

  add() {
    this.timelineService.addTimeline(this.newTitle).subscribe(timeline => {
      if (!!timeline) {
        this.router.navigateByUrl(`/timelines/${timeline.id}/edit`);
      }
    });
  }

  remove(id: string, $event: Event) {
    this.timelineService.deleteTimeline(id).subscribe();
    this.timelines = this.timelines.filter(t => t.id !== id);

    $event.preventDefault();
    $event.stopPropagation();
  }

  formatDate(date: number): string {
    return moment(new Date(date)).fromNow();
  }

  share(id: string, $event): void {
    var url = window.location.href + 'timelines/' + id;
    toastr.info(`Link to your timeline: <b><a href="${url}">${url}</a></b>`);
    $event.preventDefault();
    $event.stopPropagation();
  }

  untrack(id: string, $event): void {
    this.timelineService.stopWatching(id).subscribe(_ => {
      toastr.success('Timeline has been removed from your list.');
      this.trackedTimelines = this.trackedTimelines.filter(t => t.id !== id);
    });

    $event.preventDefault();
    $event.stopPropagation();
  }
}
