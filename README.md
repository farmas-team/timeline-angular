# About

Timeline is a website to create and share visual timelines. It was created as a learning project and it is not meant for production use.

# Features

- Integrates with Google+ to authenticate users.
- Users can create timelines of events and share a link to friends.
- Users can 'watch' their friends timelines to have them appear in their home page.

# Sample

A sample website is hosted in heroku [here](https://timeline-sample.herokuapp.com).

# Technologies Used

- [ASP.NET Core](https://www.asp.net/core/overview/aspnet-vnext)
- [Google+ Authentication](https://developers.google.com/identity)
- [MongoDB](https://www.mongodb.com)
- [Angular 2](https://angular.io)
- [Vis.js](http://visjs.org)

# Development

This website is based on the Angular SPA Template for ASP.NET Core and uses the [JavascriptServices](https://github.com/aspnet/JavaScriptServices) package to serve the Angular application.

## Setup for Local Development

- Run 'dotnet restore' from root.
- Run 'npm install ' from 'ClientApp' directory..
- Set 'ASPNETCORE_ENVIRONMENT' environment variable to 'Development'.
- Run 'npm start' from 'ClientApp' directory.
- Run 'dotnet run' from root.

## Setup for Google+ Authentication

- Go to [Google Developers Console](https://console.developers.google.com) and create a project with Google+ integration.
- Add secret or environment variable 'authentication:google:clientid' with your Google Client Id.
- Add secret or environment variable 'authentication:google:clientsecret' with your Google Client Secret.

## Setup for MongoDB Integration.

- Start a local mongo db instance. If using docker run 'docker run -it -p 27017:27017 mongo'.
- Set 'database:provider' to 'MongoDB'.
- Set 'database:mongodb:url' to connection string.
- Set 'database:mongodb:dbname' to the name of the database.
